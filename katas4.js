const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

function addToPage(text) {
    const newDiv = document.createElement('div')
    newDiv.textContent = text
    container.appendChild(newDiv)
}

function kata1() {
    addToPage(`kata1: ${JSON.stringify(gotCitiesCSV.split(','))}`)
}
kata1()

function kata2() {
    addToPage(`kata2: ${JSON.stringify(bestThing.split(' '))}`)
}
kata2()

function kata3() {
    let re = /,/gi
    addToPage(`kata3: ${JSON.stringify(gotCitiesCSV.replace(re, '; '))}`)
}
kata3()

function kata4() {
    addToPage(`kata4: ${JSON.stringify(lotrCitiesArray.join(', '))}`)
}
kata4()

function kata5() {
    addToPage(`kata5: ${JSON.stringify(lotrCitiesArray.slice(0,5))}`)
}
kata5()

function kata6() {
    addToPage(`kata6: ${JSON.stringify(lotrCitiesArray.slice(3,8))}`)
}
kata6()

function kata7() {
    addToPage(`kata7: ${JSON.stringify(lotrCitiesArray.slice(2,5))}`)
}
kata7()

function kata8() {
    lotrCitiesArray.splice(2,1)
    addToPage(`kata8: ${JSON.stringify(lotrCitiesArray)}`)
}
kata8()

function kata9() {
    lotrCitiesArray.splice(5,3)
    addToPage(`kata9: ${JSON.stringify(lotrCitiesArray)}`)
}
kata9()

function kata10() {
    lotrCitiesArray.splice(2,0,'Rohan')
    addToPage(`kata10: ${JSON.stringify(lotrCitiesArray)}`)
}
kata10()

function kata11() {
    lotrCitiesArray.splice(5,1,'Deadest Marshes')
    addToPage(`kata11: ${JSON.stringify(lotrCitiesArray)}`)
}
kata11()

function kata12() {
    addToPage(`kata12: ${JSON.stringify(bestThing.slice(0,14))}`)
}
kata12()

function kata13() {
    addToPage(`kata13: ${JSON.stringify(bestThing.slice(-12))}`)
}
kata13()

function kata14() {
    addToPage(`kata14: ${JSON.stringify(bestThing.slice(23,38))}`)
}
kata14()

function kata15() {
    addToPage(`kata15: ${JSON.stringify(bestThing.substring(bestThing.length - 12))}`)
}
kata15()

function kata16() {
    addToPage(`kata16: ${JSON.stringify(bestThing.substring(23,38))}`)
}
kata16()

function kata17() {
    addToPage(`kata17: ${JSON.stringify(bestThing.indexOf('only'))}`)
}
kata17()

function kata18() {
    addToPage(`kata18: ${JSON.stringify(bestThing.indexOf('bit'))}`)
}
kata18()

function kata19() {
    let newArr = gotCitiesCSV.split(',')
    const arr = []
    const vowels = ['aa', 'ee', 'ii', 'oo', 'uu']
    for (let city of newArr) {
        for (let vowel of vowels) {
            if (city.includes(vowel)) {
                arr.push(city)
        }
        }
    }
    addToPage(`kata 19: ${JSON.stringify(arr)}`)
}
kata19()

function kata20() {
    const arr = []
    const contain = ['or']
    for (let city of lotrCitiesArray) {
        if (city.endsWith(contain)) {
            arr.push(city)
        }
    }
    addToPage(`kata 20: ${JSON.stringify(arr)}`)
}
kata20()

function kata21() {
    const newArr = bestThing.split(' ')
    const arr = []
    for (let word of newArr) {
        if (word.startsWith('b')) {
            arr.push(word)
        }
    }
    addToPage(`kata21: ${JSON.stringify(arr)}`)
}
kata21()

function kata22() {
    if (lotrCitiesArray.includes('Mirkwood')) {
        response = 'Yes'
    } else {
        response = 'No'
    }
    addToPage(`kata 22: ${JSON.stringify(response)}`)
}
kata22()

function kata23() {
    if (lotrCitiesArray.includes('Hollywood')) {
        response = 'Yes'
    } else {
        response = 'No'
    }
    addToPage(`kata 23: ${JSON.stringify(response)}`)
}
kata23()

function kata24() {
    addToPage(`kata 24: ${JSON.stringify(lotrCitiesArray.indexOf('Mirkwood'))}`)
}
kata24()

function kata25() {
    const arr = []
    for (let name of lotrCitiesArray) {
        if (name.includes(' ')) {
            arr.push(name)
        }
    }
    addToPage(`kata 25: ${JSON.stringify(arr)}`)
}
kata25()

function kata26() {
    addToPage(`kata 26: ${JSON.stringify(lotrCitiesArray.reverse())}`)
}
kata26()

function kata27() {
    addToPage(`kata 27: ${JSON.stringify(lotrCitiesArray.sort())}`)
}
kata27()

function kata28() {
    lotrCitiesArray.sort(function(a,b) {
        return a.length - b.length
    })
    addToPage(`kata 28: ${JSON.stringify(lotrCitiesArray)}`)
}
kata28()

const lastCity = lotrCitiesArray.pop()

function kata29() {
    addToPage(`kata 29: ${JSON.stringify(lotrCitiesArray)}`)
}
kata29()

function kata30() {
    lotrCitiesArray.push(lastCity)
    addToPage(`kata 30: ${JSON.stringify(lotrCitiesArray)}`)
}
kata30()

const firstCity = lotrCitiesArray.shift()

function kata31() {
    addToPage(`kata 31: ${JSON.stringify(lotrCitiesArray)}`)
}
kata31()

function kata32() {
    lotrCitiesArray.unshift(firstCity)
    addToPage(`kata 32: ${JSON.stringify(lotrCitiesArray)}`)
}
kata32()